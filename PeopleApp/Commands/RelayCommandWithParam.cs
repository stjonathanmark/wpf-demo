﻿using System;
using System.Windows.Input;

namespace PeopleApp.Commands
{
    public class RelayCommand<T> : ICommand
    {
        readonly Action<T> execute;
        readonly Predicate<T> canExecute;

        public RelayCommand(Action<T> execute, Predicate<T> canExecute)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public RelayCommand(Action<T> execute)
           : this(execute, null)
        {
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }


        public bool CanExecute(object parameter)
        {
            return canExecute == null ? true : canExecute.Invoke((T)parameter); 
        }

        public void Execute(object parameter)
        {
            if (execute != null)
                execute.Invoke((T)parameter);
        }
    }
}
