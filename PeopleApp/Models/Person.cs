﻿
namespace PeopleApp.Models
{
    public class Person : ObservableObject
    {
        private string _firstName;
        private string _lastName;

        public string FirstName
        {
            get
            {
                return string.IsNullOrEmpty(_firstName) ? string.Empty : _firstName;
            }

            set
            {
                _firstName = value;
                OnPropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get
            {
                return string.IsNullOrEmpty(_lastName) ? string.Empty : _lastName;
            }

            set
            {
                _lastName = value;
                OnPropertyChanged("LastName");
            }
        }
    }
}
