﻿using System;
using PeopleApp.Models;
using PeopleApp.Commands;

namespace PeopleApp.ViewModels
{
    public class CreatePersonViewModel 
    {
        private CreatePerson window;
        public event Action<Person> OnCreatePerson;

        public CreatePersonViewModel(CreatePerson window)
        {
            this.window = window;
            Create = new RelayCommand(CreatePerson);
            Person = new Person();
        }

        public RelayCommand Create { get; set; }

        public Person Person { get; set; }

        public void CreatePerson()
        {
            if (OnCreatePerson != null)
                OnCreatePerson.Invoke(Person);

            window.Close();
        }
    }
}
