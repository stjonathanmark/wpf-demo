﻿using System.Collections.Generic;
using System.Windows;
using PeopleApp.Models;
using PeopleApp.Commands;
using System.Collections.ObjectModel;

namespace PeopleApp.ViewModels
{
    public class MainWindowViewModel
    {
        private CreatePersonViewModel createViewModel;
        private UpdatePersonViewModel updateViewModel;

        public MainWindowViewModel()
        {
            OpenCreate = new RelayCommand(OpenCreateWindow);
            OpenUpdate = new RelayCommand<Person>(OpenUpdateWindow);
            DeletePerson = new RelayCommand<Person>(Remove);

            People = new ObservableCollection<Person>() {
                new Person() { FirstName = "John", LastName = "Doe"},
                new Person() { FirstName = "Jane", LastName = "Doe"}
            };
        }

        public RelayCommand OpenCreate { get; set; }

        public RelayCommand<Person> OpenUpdate { get; set; }

        public RelayCommand<Person> DeletePerson { get; set; }

        public ObservableCollection<Person> People { get; set; }

        public void OpenCreateWindow()
        {
            CreatePerson cpWindow = new CreatePerson();
            createViewModel = (CreatePersonViewModel)cpWindow.DataContext;
            createViewModel.OnCreatePerson += Add;
            cpWindow.ShowDialog();
        }

        public void OpenUpdateWindow(Person person)
        {
            UpdatePerson upWindow = new UpdatePerson();
            updateViewModel = (UpdatePersonViewModel)upWindow.DataContext;
            updateViewModel.OnUpdatePerson += Update;

            updateViewModel.Index = People.IndexOf(person);

            ///
            /// Creating Deep Copy - Otherwise due to the implementation of the INotifyPropertyChanged interface in the Person model, the values 
            /// in the Row being updated on the Data Grid would update before you hit the Update button on the Update Person Window
            /// 
            /// NOTE: Would create and use a general method of making the deep copy for any object type in a real world app, but I am keeping it simple because this app is only being created for demo purposes.
            ///

            updateViewModel.Person.FirstName = person.FirstName;
            updateViewModel.Person.LastName = person.LastName;

            upWindow.ShowDialog();
        }

        public void Add(Person person)
        {
            People.Add(person);

            createViewModel = null;
        }

        public void Update(Person person)
        {
            Person personToUpdate = People[updateViewModel.Index];

            personToUpdate.FirstName = person.FirstName;
            personToUpdate.LastName = person.LastName;

            updateViewModel = null;
        }

        public void Remove(Person person)
        {
            People.Remove(person);
        }
    }
}
