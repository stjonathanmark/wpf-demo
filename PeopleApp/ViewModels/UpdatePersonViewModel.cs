﻿using System;
using PeopleApp.Commands;
using PeopleApp.Models;

namespace PeopleApp.ViewModels
{
    public class UpdatePersonViewModel
    {
        private UpdatePerson window;
        public event Action<Person> OnUpdatePerson;

        public UpdatePersonViewModel(UpdatePerson window)
        {
            this.window = window;
            Update = new RelayCommand(UpdatePerson);
            Person = new Person();
        }

        public RelayCommand Update { get; set; }
        
        public int Index { get; set; }

        public Person Person { get; set; }

        public void UpdatePerson()
        {
            if (OnUpdatePerson != null)
                OnUpdatePerson.Invoke(Person);

            window.Close();
        }
    }
}
